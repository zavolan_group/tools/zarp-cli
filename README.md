# ZARP-cli

User-friendly command-line interface for the ZARP RNA-Seq analysis pipeline

## Basic usage

```sh
zarp [-h] -f1 FILE [-f2 FILE]
```

## Parameters

```console
required arguments:
  -f1 FILE, --file-1 FILE
      file path to read/first mate library

optional arguments:
  -f2 FILE, --file-2 FILE
      file path to second mate library
  --version
      show version information and exit
  -h, --help
      show this help message and exit
```

## Installation

TBA

## Contributing

This project lives off your contributions, be it in the form of bug reports,
feature requests, discussions, or fixes and other code changes. Please refer
to the [contributing guidelines](CONTRIBUTING.md) if you are interested to
contribute. Please mind the [code of conduct](CODE_OF_CONDUCT.md) for all
interactions with the community.

## Contact

For questions or suggestions regarding the code, please use the
[issue tracker][issue-tracker]. For any other inquiries, please contact us
by email: <zavolab-biozentrum@unibas.ch>

(c) 2021 [Zavolan lab, Biozentrum, University of Basel][contact]

[contact]: <https://zavolan.biozentrum.unibas.ch/>
[issue-tracker]: <https://github.com/zavolanlab/htsinfer/issues>
