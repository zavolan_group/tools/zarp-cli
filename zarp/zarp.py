#!/usr/bin/env python
"""Infer experiment metadata from High Throughput Sequencing (HTS) data."""

import argparse
from enum import Enum
import logging
from pathlib import Path
import sys
from typing import (Any, Dict, Optional, Sequence)

from htsinfer import (
    __version__,
)

LOGGER = logging.getLogger(__name__)

class LogLevels(Enum):
    debug = logging.DEBUG
    info = logging.INFO
    warn = logging.WARNING
    warning = logging.WARNING
    error = logging.ERROR
    critical = logging.CRITICAL


def parse_args(
    args: Optional[Sequence[str]] = None
) -> argparse.Namespace:
    """Parse CLI arguments."""
    parser = argparse.ArgumentParser(
        description=sys.modules[__name__].__doc__,
    )

    parser.add_argument(
        '-f1', '--file-1',
        metavar="FILE",
        type=str,
        required=True,
        help="file path to read/first mate library",
    )
    parser.add_argument(
        '-f2', '--file-2',
        metavar="FILE",
        type=str,
        default=None,
        help="file path to second mate library",
    )
    parser.add_argument(
        '-v', '--verbosity',
        metavar="STR",
        type=str,
        default='warning',
        choices=[e.name for e in LogLevels],
        help="level of verbosity",
    )
    parser.add_argument(
        '--version',
        action='version',
        version='%(prog)s {version}'.format(version=__version__),
        help="show version information and exit",
    )

    return parser.parse_args(args)


def setup_logging(
    verbosity: str = 'warning',
) -> None:
    """Configure logging."""
    level = LogLevels[verbosity].value
    logging.basicConfig(
        level=level,
        format="[%(asctime)s] %(message)s",
        datefmt='%m-%d %H:%M:%S',
    )


def main() -> None:
    """Main function.

    Args:
        args: Command-line arguments and their values.
    """

    # Initialize
    args = parse_args()
    setup_logging(
        verbosity=args.verbosity,
    )
    LOGGER.debug("Logging set up")

    # Do stuff

    # Clean up
    LOGGER.info("Done")


if __name__ == "__main__":
    main()
